#!/usr/bin/env perl
#!/usr/bin/perl
use Config;

print "Your random number generator repeats itself after\n";
print "no more than ", 2 ** $Config{randbits}, " numbers.\n";

srand(1);

if (int(rand() * (2 ** $Config{randbits})) == 16838){
  print "Uh oh! Looks like your computer uses the ANSI example.\n";
  print "I bet the next three rands are 5758, 10113 and 17515.\n";

 foreach (1, 2, 3){
  print rand() * (2 ** $Config{randbits}), "\n";
 }
}