#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

 $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
 $tabla_articulos="articulos"; #Nombre de la tabla de articulos
 $tabla_compra="CompraArticulos";
 my $user;
 my $tabla_session_="session_";#session
 $usuario="root"; #Usuario de la BD
 my $clave="adm123"; #Password de la BD
 my $driver="mysql"; #Utilizamos el driver de mysql
 my $nombre; #Variable donde guardaremos el nombre del articulo

#Cogemos los parametros que se le pasan al CGI ($id, $nombre, $precio)
coge_parametros();

#Escribimos la cabecera de la pagina HTML'#!/usr/bin/perl
#Escribimos la cabecera de la pagina HTML

print "Content-type: text/html\n\n";

retrive_Cookie();#cookie
escribe_inicio_html();

#Conectamos con la BD, miramos si hay algun error(HACER)
$dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";



$SQL_consulta_session_="select count(*) from $tabla_session_ where a_session = ?";

$sth = $dbh->prepare("$SQL_consulta_session_");

#Realizamos la etapa de ejecución de la sentencia
$sth->execute($user);

my ($count) = $sth->fetchrow_array;

if($count==1){
    #Hacemos una consulta en la tabla, y miramos si hay algun error(HACER)
    #Mostramos los elementos de la tabla con la funcion muestra_articulo,
    #cuyo formato es:
    #                       muestra_articulo($id,$nombre,$precio);
     #Creamos la sentencia SQL
     $SQL_consulta="select distinct A.nombre,A.precio from $tabla_articulos A INNER JOIN $tabla_compra C ON A.Id=C.IdArticulo and C.user= ?;";
   
     #Realizamos la etapa de preparación de la sentencia
     $sth = $dbh->prepare("$SQL_consulta");

     #Realizamos la etapa de ejecución de la sentencia
     $sth->execute($user);

     #Realizamos la etapa de extracciÃ³n de datos. Imprimimos tupla a tupla.
     while ( @tupla=$sth->fetchrow_array() )
     {
            muestra_articulo($tupla[0],$tupla[1]);
     }

 }else{
        usuario_no_identificado($count);
 }
 #Realizamos la etapa de liberación de recursos ocupados por la sentencia
 $sth->finish();




#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
escribe_final_html();


#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#En ests funcion cogemos los parametros que son necesarios para el
#CGI.
sub coge_parametros()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }

        

}

#En esta funcion muestra el articulo que se le pasa como parametro
#el formato es muestra_articulo($id,$nombre,$precio);
sub muestra_articulo()
{
        my ($nombre,$precio,$total)=@_;

print <<articulo_HTML;

	<TR>
         
         <strong>
         
         <TD rowspan=2 valign=middle><font color="#000000" face="sans-serif size=+2">$nombre</font></TD>
         <TD rowspan=2 valign=middle><font color="#000000" face="sans-serif size=+2">$precio</font></TD>
	 </strong>

	</TR>

	<!-- Linea en blanco-->
	<TR><TD colspan=3 valign=middle bgcolor="#FFFFFF"></TD></TR>
articulo_HTML
}

#Terminamos
exit;

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Informe de compra</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Carro de compras</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
	<hr>
	<br>
    <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
        <TR><TD><form action="logout.pl" method="get">
        <TABLE>
        <TR><TD width="100%">
        <TD>
        <INPUT TYPE="submit" VALUE="salir del sistema">
        </TD>
        </TR>
        </TABLE>
        </form>
        </TD></TR>
    </TABLE>  
         
        <form action="cgi_ListaArticulos.pl" method="get">
        <TABLE> 
       <TR>
        <TD>
		<INPUT TYPE="submit" VALUE="Listado articulos">
	</TD>
       </TR>
       </TABLE>
      </form>         


        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>
	<TR>
         <font color="#AAAAAA" face="sans-serif"><strong>
         <TH valign=middle bgcolor="#ff4000">Nombre</TH>
         <TH valign=middle bgcolor="#ff4000">Precio</TH>
	 </strong></font>
	</TR>

inicio_HTML
}

#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
<form action="CompraFinal.pl" method="get">
<TABLE>
<TR>
<TD>
<INPUT TYPE="submit" VALUE="Procesar">
</TD>
</TR>
</TABLE>
</form>
	</TABLE>
        </BODY>
        </HTML>
fin_HTML
}



#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub usuario_no_identificado()
{
my ($count)=@_;

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Usuario no identificado dirijase al login</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Usuario no identificado dirijase al login</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
            <form action="ht_valida_cliente.pl" method="get">
                <TABLE align=center>
                 <TR>
                  <TD >
                   <INPUT TYPE="submit" VALUE="ir al login">$count
                  </TD>
                 </TR>
                </TABLE>
           </form>       
         </TD>
        </TABLE>
        <hr>
        <br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}

sub retrive_Cookie()
{
$rcvd_cookies = $ENV{'HTTP_COOKIE'};
 @cookies = split /;/, $rcvd_cookies;
foreach $cookie ( @cookies ){
   ($key, $val) = split(/=/, $cookie); # splits on the first =.
   $key =~ s/^\s+//;
   $val =~ s/^\s+//;
   $key =~ s/\s+$//;
   $val =~ s/\s+$//;
   if( $key eq "user" ){
      $user = $val;
   }
}
print "usuario logueado  = $user\n";
}