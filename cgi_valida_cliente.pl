#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

 $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
 $tabla_clientes="clientes"; #Nombre de la tabla de clientes
 $tabla_session= "session_";
 $usuario="root"; #Usuario de la BD
 $clave="adm123"; #Password de la BD
 $driver="mysql"; #Utilizamos el driver de mysql

my $user;     #VAriable donde guardaremos el user del cliente
my $pass;     #VAriable donde guardaremos el user del cliente



#Cogemos los parametros que se le pasan al CGI ($user, $pass, $name, $email, $direccion, $telefono)
coge_parametros();
print "Set-Cookie:user=$user; Max-Age=600;\n";

#Escribimos la cabecera de la pagina HTML'#!/usr/bin/perl
#Escribimos la cabecera de la pagina HTML

print "Content-type: text/html\n\n";

#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
            || die "\nError al abrir la base datos: $DBI::errstr\n";

#Creamos la sentencia SQL
$SQL_consulta_session_="select count(*) from $tabla_session where a_session = ?";

#Realizamos la etapa de preparaci�n de la sentencia
$sth = $dbh->prepare("$SQL_consulta_session_");

#Realizamos la etapa de ejecuci�n de la sentencia
$sth->execute($user);

my ($countsession) = $sth->fetchrow_array;
#valida si el usuario se autentico.
my $count;

if($countsession==1){
    print '';
    #Hacemos una consulta en la tabla, y miramos si hay algun error(HACER)
    #Mostramos los elementos de la tabla con la funcion valida_usuario,
    #cuyo formato es:valida_usuario($count);
    valida_user($user,$pass);
#inserta registro session
    if($count==1){
    #Realizamos la etapa de extracción de datos. Imprimimos tupla a tupla.
            valida_usuario($count,$user);
    }else{
            usuario_no_identificado($count);
    }

}else {
    retrive_Cookie();
    print '';
    valida_user($user,$pass);

    if($count==1){

        my $SQL_inserta_="insert into $tabla_session (a_session) values ('$user');";

        #Insertamos los datos en la tabla, miramos si hay algun error(HACER)
        $resultado = $dbh->do ("$SQL_inserta_");
    
    #Realizamos la etapa de extracción de datos. Imprimimos tupla a tupla.
            valida_usuario($count,$user);
    }else{
            usuario_no_identificado($count);
    }
}
 
 #Realizamos la etapa de liberaci�n de recursos ocupados por la sentencia
 $sth->finish();

#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
#escribe_final_html();

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En ests funcion cogemos los parametros que son necesarios para el
#CGI.
sub coge_parametros()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }

        $user=$datos{USER};
        $pass=$datos{PASS};
}

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En esta funcion muestra el articulo que se le pasa como parametro
#el formato es muestra_articulo($id,$nombre,$precio);
sub valida_usuario()
{
        my ($count,$usuario)=@_;

print <<articulo_HTML;
        
Content-type: text/html\n\n
<html>
<head>
<title>Insertar articulo</title>
<body>

<TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Insertar articulo</h1>
                 </strong>
                </font>
         </TD>
</TABLE>
</TABLE>
    <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
    <TR><TD><form action="logout.pl" method="get">
    <TABLE>
    <TR><TD width="100%">
    <TD>
    <INPUT TYPE="submit" VALUE="salir del sistema">
    </TD>
    </TR>
    </TABLE>
    </form>
    </TD></TR>
</TABLE>

<form action="cgi_ListaArticulos.pl" method="get">
<TABLE>
<TR>
<TD>
<INPUT TYPE="submit" VALUE="Lista articulos">
</TD>
</TR>
</TABLE>
</form>
<form action="cgi_InsertarArticulo.pl" method="get">
<TABLE BORDER=0 cellspacing=1 >
<TR>
<TH align="left">Nombre:</TH>
<TD><INPUT TYPE="text" NAME="NOMBRE" SIZE=30 MAXLENGTH=199></TD>
<TR>
<TH align="left">Precio:</TH>
<TD><INPUT TYPE="text" NAME="PRECIO" SIZE=30 MAXLENGTH=30></TD>
<TR>
<TH align="left">Descripcion:</TH>
<TD><INPUT TYPE="text" NAME="DESCRIPCION" SIZE=30 MAXLENGTH=30></TD>
     <TR>
     <TR>
     <TD>
         <INPUT TYPE="submit" VALUE="Aceptar">
     <TD>
     </TR>   
articulo_HTML

print <<fin_HTML
        </TABLE>

         </TABLE>
            <TABLE BGCOLOR="#ffffff" BORDER=1>
            <TR>
            <TD>VALIDACIONES</TD>
            </TR>
            <TR>
            <TD>
          * el campo de nombre tiene un minimo(8)/maximo(190) de letras. Solo permite letras</BR>
          **el campo de precio solo debe contener numeros</BR>
          ***el campo de descripcion debe tener como minimo(10)/maximo(190) de caracteres. Ademas, solo permite letras o numeros</BR> 
          </TD>
          </TR>

          </TABLE>
        </BODY>
        </HTML>
fin_HTML


}

#Terminamos
exit;

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Cliente (CGI)</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>valida usuario 1 (CGI)</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
	<hr>
	<br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>
	<TR>
         <font color="#AAAAAA" face="sans-serif"><strong>
         <TH valign=middle bgcolor="#008caa">valor session</TH>
	 </strong></font>
	</TR>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}



#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub usuario_no_identificado()
{
my ($count)=@_;

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Usuario no identificado dirijase al login</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Usuario no identificado dirijase al login</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
            <form action="ht_valida_cliente.pl" method="get">
                <TABLE align=center>
                 <TR>
                  <TD >
                   <INPUT TYPE="submit" VALUE="ir al login">$count
                  </TD>
                 </TR>
                </TABLE>
           </form>       
         </TD>
        </TABLE>
        <hr>
        <br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
	</TABLE>
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------

sub valida_user()
{
    my ($user,$pass)=@_;

    #Creamos la sentencia SQL

    $SQL_consulta="select CAST(AES_DECRYPT(pass,'F3229A0B371ED2D8441B830321A392C3')AS CHAR (20)) from $tabla_clientes where user = ?";
    #Realizamos la etapa de preparaci�n de la sentencia
    $sth = $dbh->prepare("$SQL_consulta");
    print " ";
     #Realizamos la etapa de ejecuci�n de la sentencia
    $sth->execute($user);

     #Realizamos la etapa de extracción de datos. Imprimimos tupla a tupla.
     while ( @tupla=$sth->fetchrow_array() )
     {
        print "";
        #valida si usuario es valido
           if($pass eq $tupla[0]){
            print "";
            $count = 1;
    
           }else{
            print "";
            $count = 0;
           }
     }
}

sub retrive_Cookie()
{
$rcvd_cookies = $ENV{'HTTP_COOKIE'};
 @cookies = split /;/, $rcvd_cookies;
foreach $cookie ( @cookies ){
   ($key, $val) = split(/=/, $cookie); # splits on the first =.
   $key =~ s/^\s+//;
   $val =~ s/^\s+//;
   $key =~ s/\s+$//;
   $val =~ s/\s+$//;
   if( $key eq "user" ){
      $user = $val;
   }
}
print "usuario logueado  = $user\n";
}