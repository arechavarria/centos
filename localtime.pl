#!/usr/bin/perl
#!c:\perl\bin\perl 

use strict;

use vars qw($segundos $minutos $horas
            $dia_mes $mes $anyo $dia_semana
            $dd $mm $yy $yyyy
            $HH $MM $am_pm
            $dia_semana_castellano $mes_castellano
            $fecha_unix);


### Aqui empieza el codigo interesante ###


$fecha_unix = time ();  # Comenzamos obteniendo la fecha/hora actual



# Ahora le pedimos a localtime() que nos ayude a procesar la fecha,
# para contar con valores mas comodos. Los campos que esta funcion
# retorna y no necesitamos podemos ignorarlos. En este caso `undef'
# permite hacer tal cosa.

($segundos, $minutos, $horas,
 $dia_mes, $mes, $anyo, $dia_semana,
 undef, undef) = localtime ($fecha_unix);


# Ahora realizamos algunos procesamientos propios... Todo depende de
# como queremos disponer de la fecha/hora.

# Primero tomemos los valores de dia y mes, asegurando que cuenten con
# dos digitos.

$dd = $dia_mes < 10 ? '0' . $dia_mes : $dia_mes;

$mm = $mes < 10 ? '0' . $mes : $mes;

if ($anyo < 1900)
    { $anyo += 1900 }  # Con el proposito de evitar el `bug' del milenio

$yy = substr ($anyo, 2, 2);


# Ahora produzcamos tambien otros formatos para las horas

$MM = $minutos < 10 ? '0' . $minutos+5 : $minutos;

if ($horas > 12) {
    $HH = $horas - 12 < 10 ? '0' . ($horas - 12) : $horas - 12;
    $am_pm = 'PM';
} else {
    $HH = $horas < 10 ? '0' . $horas : $horas;
    $am_pm = 'pm';
}


# Finalmente, vamos a preparar un formato personalizado, y en este
# caso, "castellanizado". Es posible, para este proposito, utilizar
# ciertas funciones que dependen de la forma en que se encuentre
# configurado el sistema. Sin embargo, estas por obvias razones pueden
# producir resultados diferentes en maquinas diferentes; asi que
# preferiremos implementar nuestra propia solucion.

for ($dia_semana) {
    /^0$/ && do { $dia_semana_castellano = 'Domingo';   last; };
    /^1$/ && do { $dia_semana_castellano = 'Lunes';     last; };
    /^2$/ && do { $dia_semana_castellano = 'Martes';    last; };
    /^3$/ && do { $dia_semana_castellano = 'Miercoles'; last; };
    /^4$/ && do { $dia_semana_castellano = 'Jueves';    last; };
    /^5$/ && do { $dia_semana_castellano = 'Viernes';   last; };
    /^6$/ && do { $dia_semana_castellano = 'Sabado';    last; };
}

for ($mes) {
    /^1$/  && do { $mes_castellano = 'Enero';      last; };
    /^2$/  && do { $mes_castellano = 'Febrero';    last; };
    /^3$/  && do { $mes_castellano = 'Marzo';      last; };
    /^4$/  && do { $mes_castellano = 'Abril';      last; };
    /^5$/  && do { $mes_castellano = 'Mayo';       last; };
    /^6$/  && do { $mes_castellano = 'Junio';      last; };
    /^7$/  && do { $mes_castellano = 'Julio';      last; };
    /^8$/  && do { $mes_castellano = 'Agosto';     last; };
    /^9$/  && do { $mes_castellano = 'Septiembre'; last; };
    /^10$/ && do { $mes_castellano = 'Octubre';    last; };
    /^11$/ && do { $mes_castellano = 'Noviembre';  last; };
    /^12$/ && do { $mes_castellano = 'Diciembre';  last; };
}


print "Content-type: text/html\n\n";
# Ahora vamos a mostrar la fecha, en algunos formatos comunes.

print "\nEn este momento, la fecha/hora del sistema es la siguiente:\n\n";

print "  UNIX timestamp : $fecha_unix\n";
print "  D-M-Y H:M:S    : $dia_mes-$mes-$anyo $horas:$minutos:$segundos\n";
print "  DD/MM/YY       : $dd/$mm/$yy\n";
print "  Personalizado  : $dia_semana_castellano $dia_mes " .
    "de $mes_castellano, $anyo $HH:$MM $am_pm\n";