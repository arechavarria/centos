#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_articulos="articulos"; #Nombre de la tabla de articulos
my $tabla_compra="CompraArticulos"; #Nombre de la tabla de articulos
my $user;
my $tabla_session_="session_";#session
my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql


my $id;     #VAriable donde guardaremos el id del articulo

#Cogemos el parametro que se le pasan al CGI ($id)
$id=coge_parametro();

#Creamos la sentencia SQL
my $SQL_borra="delete from $tabla_articulos where (ID=$id);";
my $SQL_borraCompra="delete from $tabla_compra where (IdArticulo=$id);";

#Escribimos la cabecera de la pagina HTML
print "Content-type: text/html\n\n";
retrive_Cookie();#cookie
escribe_inicio_html();

#Escrimos los parametros pasados (BORRAR)

#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";

$SQL_consulta_session_="select count(*) from $tabla_session_ where a_session = ?";

$sth = $dbh->prepare("$SQL_consulta_session_");

#Realizamos la etapa de ejecución de la sentencia
$sth->execute($user);

my ($count) = $sth->fetchrow_array;
print 'paso por aqui*1';
if($count==1){
    print 'paso por aqui*2';
    #Borramos el dato de la tabla, miramos si hay algun error(HACER)
    $resultado = $dbh->do ("$SQL_borra");
    $resultado2 = $dbh->do ("$SQL_borraCompra");

    if ($resultado!=0)
    {
    print '<form action="cgi_ListaArticulos.pl" method="get">';
    print '<TABLE>';
    print '<TR>';
    print '<TD>';
    print '<INPUT TYPE="submit" VALUE="Listado articulos">';
    print '</TD>';
    print '</TR>';
    print '</TABLE>';
    print '</form>';
    	print ("\n<h3>Articulo $id <b>Borrado exitosamente</b><h3><br>\n");
    }
    else
    {
    	print "\n<h3>Art&iacute;culo $id <b>NO BORRADO</b><h3><br>\n";
    	print "\n<b>ERROR</b> en borrado en $tabla_articulos: $DBI::errstr<br>\n";
    }


}else{
        usuario_no_identificado($count);
}

#Realizamos la etapa de liberación de recursos ocupados por la sentencia

$sth->finish();

#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
escribe_final_html();

#Terminamos
exit;
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En ests funcion cogemos el parametro necesario para el
#CGI.
sub coge_parametro()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }

        return $datos{ID};
}

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Borrado de un arti&aciute;lo (CGI)</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#ff8000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Borrado de un articulo</h1>
                 </strong>

         </TD>
        </TABLE>
inicio_HTML
}

#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------


#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub usuario_no_identificado()
{
my ($count)=@_;

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Usuario no identificado dirijase al login</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Usuario no identificado dirijase al login</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
            <form action="ht_valida_cliente.pl" method="get">
                <TABLE align=center>
                 <TR>
                  <TD >
                   <INPUT TYPE="submit" VALUE="ir al login">$count
                  </TD>
                 </TR>
                </TABLE>
           </form>       
         </TD>
        </TABLE>
        <hr>
        <br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}

sub retrive_Cookie()
{
$rcvd_cookies = $ENV{'HTTP_COOKIE'};
 @cookies = split /;/, $rcvd_cookies;
foreach $cookie ( @cookies ){
   ($key, $val) = split(/=/, $cookie); # splits on the first =.
   $key =~ s/^\s+//;
   $val =~ s/^\s+//;
   $key =~ s/\s+$//;
   $val =~ s/\s+$//;
   if( $key eq "user" ){
      $user = $val;
   }
}
print "usuario logueado  = $user\n";
}
