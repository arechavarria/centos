#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

 $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
 $tabla_articulos="articulos"; #Nombre de la tabla de articulos
 my $tabla_session_="session_"; #Nombre de la tabla de session

 my $user;
 $usuario="root"; #Usuario de la BD
 my $clave="adm123"; #Password de la BD
 my $driver="mysql"; #Utilizamos el driver de mysql
 my $nombre; #Variable donde guardaremos el nombre del articulo

#Cogemos los parametros que se le pasan al CGI ($id, $nombre, $precio)
coge_parametros();


print "Content-type: text/html\n\n";


#Creamos la sentencia SQL
$SQL_consulta="select * from $tabla_articulos where nombre like '%$nombre%'";

#Conectamos con la BD, miramos si hay algun error(HACER)
$dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
        || die "\nError al abrir la base datos: $DBI::errstr\n";

escribe_inicio_html();
#Hacemos una consulta en la tabla, y miramos si hay algun error(HACER)
#Mostramos los elementos de la tabla con la funcion muestra_articulo,
#cuyo formato es:
#                       muestra_articulo($id,$nombre,$precio);

 #Realizamos la etapa de preparaci�n de la sentencia
 $sth = $dbh->prepare("$SQL_consulta");


 #Realizamos la etapa de ejecuci�n de la sentencia
 $sth->execute();
 #Realizamos la etapa de extracción de datos. Imprimimos tupla a tupla.
 while ( @tupla=$sth->fetchrow_array() )
 {
        muestra_articulo($tupla[0],$tupla[1],$tupla[2]);
 }


 #Realizamos la etapa de liberaci�n de recursos ocupados por la sentencia
     $sth->finish();

    #Nos desconectamos de la BD. (HACER)
    $dbh->disconnect();

    #Escribimos la cola de la pagina HTML
    escribe_final_html();

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#En ests funcion cogemos los parametros que son necesarios para el
#CGI.
sub coge_parametros()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }

        $nombre=$datos{NOMBRE};

}

#En esta funcion muestra el articulo que se le pasa como parametro
#el formato es muestra_articulo($id,$nombre,$precio);
sub muestra_articulo()
{
        my ($id,$nombre,$precio)=@_;

print <<articulo_HTML;

	<TR>
         <!-- Boton de Ver detalle producto -->
        <TD valign=middle bgcolor="#ffffff">
                <FORM NAME="formCompra$id" ACTION="ht_DetalleArticulo_.pl" METHOD=GET>
                        <INPUT TYPE="hidden" NAME="ID" VALUE="$id">
                        <INPUT TYPE="hidden" NAME="NOMBRE" VALUE="1">
                </FORM>
        </TD>
         <strong>
         <TD rowspan=2 valign=middle><font color="#000000" face="sans-serif size=+2">$id</font></TD>
         <TD rowspan=2 valign=middle><font color="#000000" face="sans-serif size=+2">$nombre</font></TD>
         <TD rowspan=2 valign=middle><font color="#000000" face="sans-serif size=+2">$precio</font></TD>
	 </strong>

	</TR>

	<!-- Linea en blanco-->
	<TR><TD colspan=3 valign=middle bgcolor="#FFFFFF"></TD></TR>
articulo_HTML
}

#Terminamos
exit;

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Listar de productos</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Home</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100% align="rigth">
        <TR><TD>
        <TABLE>
        <TR>
        <TD>
        	<h2><a href="ht_valida_cliente.pl">Login</a></h2>
        </TD>
        <TD>
        	<h2><a href="ht_inserta_cliente.pl">Registrarse</a></h2>
        </TD>
        <TD>
        	<h2><a href="cgi_listaComentarios.pl">Ir al Blog</a></h2>
        </TD>
        </TR>
        </TABLE>
        </TD></TR>
        </TABLE>
	<hr>
	<br>
         
          <FORM NAME="ListarFiltro" ACTION="cgi_ListaArticulosInvitados.pl" METHOD=GET>
         <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="ff4000">           
	   
		<INPUT TYPE="text" NAME="NOMBRE" SIZE=30 MAXLENGTH=30>
           
		<INPUT TYPE="submit" VALUE="Buscar">	
	           
         </TD>
        </TABLE>
         </FORM>
	<hr>
	<br>
</form> 

        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>
	<TR>
         <font color="#AAAAAA" face="sans-serif"><strong>
         <TH valign=middle bgcolor="#ff4000"></TH>
         <TH valign=middle bgcolor="#ff4000">Articulo</TH>
         <TH valign=middle bgcolor="#ff4000">Nombre</TH>
         <TH valign=middle bgcolor="#ff4000">Precio</TH>
	 </strong></font>
	</TR>

inicio_HTML
}


#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub usuario_no_identificado()
{
my ($count)=@_;

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Usuario no identificado dirijase al login</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Usuario no identificado dirijase al login</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
            <form action="ht_valida_cliente.pl" method="get">
                <TABLE align=center>
                 <TR>
                  <TD >
                   <INPUT TYPE="submit" VALUE="ir al login">
                  </TD>
                 </TR>
                </TABLE>
           </form>       
         </TD>
        </TABLE>
        <hr>
        <br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}

