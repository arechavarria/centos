#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_articulos="articulos"; #Nombre de la tabla de articulos
my $tabla_pedidos="pedidos";   #Nombre de la tabla de pedidos
my $tabla_clientes="clientes";   #Nombre de la tabla de clientes

my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql

#Creamos 2 variables con las sentencias SQL que borran las tablas
my $SQL_borra_tabla_articulos="drop table $tabla_articulos;";
my $SQL_borra_tabla_pedidos="drop table $tabla_pedidos;";
my $SQL_borra_tabla_clientes="drop table $tabla_clientes;";


#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";


#Borramos las dos tabla, miramos si hay algun error(HACER)
$dbh->do("$SQL_borra_tabla_articulos") || die "\nError en borrado de tabla $tabla_articulos: $DBI::errstr\n";
$dbh->do("$SQL_borra_tabla_pedidos")   || die "\nError en borrado de tabla $tabla_pedidos: $DBI::errstr\n";
$dbh->do("$SQL_borra_tabla_clientes")   || die "\nError en borrado de tabla $tabla_clientes: $DBI::errstr\n";

#Si todo ha ido bien, lo decimos
print "\n Se han borrado las tablas $tabla_articulos,$tabla_clientes y $tabla_pedidos\n";

#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Terminamos
exit;
