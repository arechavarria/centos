#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_clientes="clientes"; #Nombre de la tabla de clientes

my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql


my $id;     #VAriable donde guardaremos el id del cliente

#Cogemos el parametro que se le pasan al CGI ($id)
$id=coge_parametro();

#Creamos la sentencia SQL
my $SQL_borra="delete from $tabla_clientes where (ID=$id);";


#Escribimos la cabecera de la pagina HTML
print "Content-type: text/html\n\n";
escribe_inicio_html();

#Escrimos los parametros pasados (BORRAR)
#print ("\n<h2>Hay que borrar el cliente Id=$id<h2><br>\n");


#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";


#Borramos el dato de la tabla, miramos si hay algun error(HACER)
$resultado = $dbh->do ("$SQL_borra");

if ($resultado!=0)
{
	print ("\n<h3>Cliente $id <b>Borrado</b><h3><br>\n");
}
else
{
	print "\n<h3>Cliente $id <b>NO BORRADO</b><h3><br>\n";
	print "\n<b>ERROR</b> en borrado en $tabla_clientes: $DBI::errstr<br>\n";
}

#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
escribe_final_html();

#Terminamos
exit;
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En ests funcion cogemos el parametro necesario para el
#CGI.
sub coge_parametro()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }

        return $datos{ID};
}

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Borrado de un cliente (CGI)</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>BORRADO DE UN CLIENTE(CGI)</h1>
                 </strong>

         </TD>
	<form action="cgi_lista_cliente.pl" method="post">
         <TABLE>
          <TR>
           <TD>
            <INPUT TYPE="submit" VALUE="Volver a lista clientes">
           </TD>
          </TR>
         </TABLE>
        </form>
        </TABLE>
inicio_HTML
}

#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------

