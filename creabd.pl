#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_articulos="articulos"; #Nombre de la tabla de articulos
my $tabla_pedidos="pedidos";   #Nombre de la tabla de pedidos
my $tabla_clientes="clientes";   #Nombre de la tabla de clientes
my $tabla_sessions="sessions";   #Nombre de la tabla de clientes

#Campos de la tabla articulos
my $campos_tabla_articulos=" id INT PRIMARY KEY, nombre VARCHAR(200) NOT NULL, precio FLOAT";

#Campos de la tabla de pedidos
my $campos_tabla_pedidos=" num_factura INT PRIMARY KEY,".
			 " id INT,".
			 " nombre VARCHAR(200) NOT NULL,".
		         " email VARCHAR(100) NOT NULL, ".
			 " direccion VARCHAR(200),".
			 " telefono VARCHAR(15)";

#Campos de la tabla de clientes
my $campos_tabla_clientes=" id  INT PRIMARY KEY AUTO_INCREMENT,".
			  " user  VARCHAR(30) NOT NULL,".
			  " pass VARCHAR(20) NOT NULL,".
		          " nombre VARCHAR(200)  NOT NULL, ".
		          " email VARCHAR(100) NOT NULL,".
			  " direccion VARCHAR(200),".
			  " telefono VARCHAR(15)";

my $campos_tabla_sessions=" id  INT PRIMARY KEY AUTO_INCREMENT,".
			  " a_session tex NOT NULL";

my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql

#Creamos las 3 variables con las sentencias SQL que crean las tablas
my $SQL_crea_tabla_articulos="create table $tabla_articulos ($campos_tabla_articulos);";
my $SQL_crea_tabla_pedidos="create table $tabla_pedidos ($campos_tabla_pedidos);";
my $SQL_crea_tabla_clientes="create table $tabla_clientes ($campos_tabla_clientes);";
my $SQL_crea_tabla_sessions="create table $tabla_sessions ($campos_tabla_sessions;";


#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";



#Creamos las tres tablas, miramos si hay algun error(HACER)
$dbh->do("$SQL_crea_tabla_articulos") || die "\nError en creacion de tabla $tabla_articulos: $DBI::errstr\n";
$dbh->do("$SQL_crea_tabla_pedidos")   || die "\nError en creacion de tabla $tabla_pedidos: $DBI::errstr\n";
$dbh->do("$SQL_crea_tabla_clientes")   || die "\nError en creacion de tabla $tabla_clientes: $DBI::errstr\n";
$dbh->do("$SQL_crea_tabla_sessions")   || die "\nError en creacion de tabla $tabla_sessions: $DBI::errstr\n";



#Si todo ha ido bien, lo decimos
print "\n Se han creado las tablas $tabla_clientes, $tabla_articulos,$tabla_pedidos y $tabla_sessions\n";


#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();


#Terminamos
exit;

