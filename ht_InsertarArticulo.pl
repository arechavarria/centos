#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_session_="session_"; #Nombre de la tabla de articulos

my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql

my $user;

print "Content-type: text/html\n\n";

obtiene_usuario_logueado();

#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
        || die "\nError al abrir la base datos: $DBI::errstr\n";

#Creamos la sentencia SQL
$SQL_consulta_session_="select count(*) from $tabla_session_ where a_session = ?";

#Realizamos la etapa de preparación de la sentencia
$sth = $dbh->prepare("$SQL_consulta_session_");

#Realizamos la etapa de ejecución de la sentencia
$sth->execute($user);

print '';

my $countsession = $sth->fetchrow_array;

print '';

#valida si el usuario se autentico.
if($countsession==1){
  print '';


  print '<html>';
  print '<head>';
  print '<title>Insertar articulo</title>';
  print '<body>';

  print '<TABLE border=0  cellpadding=0 cellspacing=0 width=100%>';
  print '         <TR><TD valign=middle width="100%" bgcolor="#000000">';
  print '                <font color="#DDDDDD" face="sans-serif">';
  print '                 <strong>';
  print "                        <h1 align=center style='text-align:center'>Insertar articulo</h1>";
  print '                 </strong>';
  print '                </font>';
  print '         </TD></TR>';
  print '        </TABLE>';

  print '<TABLE border=0  cellpadding=0 cellspacing=0 width=100%>';
  print '<TR><TD><form action="logout.pl" method="get">';
  print '<TABLE>';
  print '<TR><TD width="100%">';
  print '<TD>';
  print '<INPUT TYPE="submit" VALUE="salir del sistema">';
  print '</TD>';
  print '</TR>';
  print '</TABLE>';
  print '</form>';
  print '</TD></TR>';
  print '        </TABLE>';

  print '<form action="cgi_ListaArticulos.pl" method="get">';
  print '<TABLE>';
  print '<TR>';
  print '<TD>';
  print '<INPUT TYPE="submit" VALUE="Lista articulos">';
  print '</TD>';
  print '</TR>';
  print '</TABLE>';
  print '</form>';
  print '<form action="cgi_InsertarArticulo.pl" method="get">';
  print '<TABLE BORDER=0 cellspacing=1 >';
  print '            <TR>';
  print ' 	    <TH align="left">Nombre:</TH>';
  print '		<TD><INPUT TYPE="text" NAME="NOMBRE" SIZE=30 MAXLENGTH=199></TD>';
  print '            <TR>';
  print '            <TH align="left">Precio:</TH>';
  print '		<TD><INPUT TYPE="text" NAME="PRECIO" SIZE=30 MAXLENGTH=30></TD>';
  print '            <TR>';
  print '            <TH align="left">Descripcion:</TH>';
  print '         <TD><INPUT TYPE="text" NAME="DESCRIPCION" SIZE=30 MAXLENGTH=30></TD>';
  print '	    <TR> ';
  print '     <TR>';
  print '     <TD>';
  print '         <INPUT TYPE="submit" VALUE="Aceptar">';
  print '     <TD>';
  print '     </TR>';		
  print '	  </TABLE>';
  print '</form>';


  print '   </TABLE>';
  print '          <TABLE BGCOLOR="#ffffff" BORDER=1';

  print '    <TR>';

  print '    <TD>VALIDACIONES</TD>';

  print '    </TR>';
  print '    <TR>';
  print '    <TD>';
  print '* el campo de nombre tiene un minimo(8)/maximo(190) de letras. Solo permite letras</BR>';
  print '**el campo de precio solo debe contener numeros</BR>';
  print '***el campo de descripcion debe tener como minimo(10)/maximo(190) de caracteres. Ademas, solo permite letras o numeros</BR>'; 
  print '</TD>';
  print '</TR>';

  print '   </TABLE>';
  print '</body>';
  print '</html>';

}else{
	 usuario_no_identificado($count);
	 print '';
}
#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Terminamos
exit;

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub usuario_no_identificado()
{
my ($count)=@_;

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Usuario no identificado dirijase al login</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Usuario no identificado dirijase al login</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
            <form action="ht_valida_cliente.pl" method="get">
                <TABLE align=center>
                 <TR>
                  <TD >
                   <INPUT TYPE="submit" VALUE="ir al login">$count
                  </TD>
                 </TR>
                </TABLE>
           </form>       
         </TD>
        </TABLE>
        <hr>
        <br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}



sub obtiene_usuario_logueado()
{
$rcvd_cookies = $ENV{'HTTP_COOKIE'};
@cookies = split /;/, $rcvd_cookies;
foreach $cookie ( @cookies ){
   ($key, $val) = split(/=/, $cookie); # splits on the first =.
   $key =~ s/^\s+//;
   $val =~ s/^\s+//;
   $key =~ s/\s+$//;
   $val =~ s/\s+$//;
   if( $key eq "user" ){
      $user = $val;
   }
}
print "usuerio logueado = $user\n";
}
