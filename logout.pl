#!/usr/bin/perl
#!c:\perl\bin\perl

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_session_="session_"; #Nombre de la tabla de articulos

my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql

my $user;
my $resetuser;
my $id;
my $nombre; #Variable donde guardaremos el nombre del articulo
my $precio; #Variable donde guardaremos el precio del articulo
my $descripcion; 

#Cogemos los parametros que se le pasan al CGI ($id, $nombre, $precio)
coge_parametros();

#Escribimos la cabecera de la pagina HTML
print "Set-Cookie:user=; Max-Age=0;\n";
print "Content-type: text/html\n\n";
#obtiene cookies.
retrive_Cookie();

#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
        || die "\nError al abrir la base datos: $DBI::errstr\n";

#Creamos la sentencia SQL para insertar articulos
my $SQL_delete="delete from $tabla_session_ where a_session = '$user';";


#Insertamos los datos en la tabla, miramos si hay algun error(HACER)
$resultado = $dbh->do ("$SQL_delete");


if ($resultado)
{
	print "Set-Cookie:user=$user; Max-Age=0;\n";

}

usuario_no_identificado();
#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
escribe_final_html();

#Terminamos
exit;
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En ests funcion cogemos los parametros que son necesarios para el
#CGI.
sub coge_parametros()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }
        $user=$datos{USER};
        $nombre=$datos{NOMBRE};
        $precio=$datos{PRECIO};
        $descripcion=$datos{DESCRIPCION};
        
}

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Insercion de articulo</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#ff8000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Resultado de la transaccion</h1>
                 </strong>

         </TD>
        </TABLE>
inicio_HTML
}


#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub usuario_no_identificado()
{
my ($count)=@_;

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Usuario no identificado dirijase al login</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>Usuario no identificado dirijase al login</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#007b99">
            <form action="ht_valida_cliente.pl" method="get">
                <TABLE align=center>
                 <TR>
                  <TD >
                   <INPUT TYPE="submit" VALUE="ir al login">$count
                  </TD>
                 </TR>
                </TABLE>
           </form>       
         </TD>
        </TABLE>
        <hr>
        <br>
        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>

inicio_HTML

print <<fin_HTML;
        </TABLE>
        </BODY>
        </HTML>
fin_HTML
}

sub retrive_Cookie()
{
$rcvd_cookies = $ENV{'HTTP_COOKIE'};
 @cookies = split /;/, $rcvd_cookies;
foreach $cookie ( @cookies ){
   ($key, $val) = split(/=/, $cookie); # splits on the first =.
   $key =~ s/^\s+//;
   $val =~ s/^\s+//;
   $key =~ s/\s+$//;
   $val =~ s/\s+$//;
   if( $key eq "user" ){
      $user = $val;
   }
}
print "usuario logueado  = $user\n";
}

#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------

