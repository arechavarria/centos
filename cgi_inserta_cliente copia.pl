#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

my $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
my $tabla_clientes="clientes"; #Nombre de la tabla de clientes

my $usuario="root"; #Usuario de la BD
my $clave="adm123"; #Password de la BD
my $driver="mysql"; #Utilizamos el driver de mysql


my $user;     #VAriable donde guardaremos el user del cliente
my $pass;     #VAriable donde guardaremos el user del cliente
my $nombre; #Variable donde guardaremos el nombre del cliente
my $email; #Variable donde guardaremos el email del cliente
my $direccion; #Variable donde guardaremos la direccion del cliente
my $telefono; #Variable donde guardaremos el telefono del cliente

#Cogemos los parametros que se le pasan al CGI ($user, $pass, $name, $email, $direccion, $telefono)
coge_parametros();

#Creamos la sentencia SQL para insertar
my $SQL_inserta="insert INTO $tabla_clientes (user,pass, nombre, email, direccion,telefono) values ('$user','$pass', '$nombre','$email','$direccion','$telefono');";


#Escribimos la cabecera de la pagina HTML
print "Content-type: text/html\n\n";
escribe_inicio_html();

#Escrimos los parametros pasados (BORRAR)
print ("\n<h3>Id=$user <h3><br>\n");
print ("\n<h3>Nombre=$nombre<h3><br>\n");
print ("\n<h3>Email=$email<h3><br>\n");
print ("\n<h3>Direccion=$direccion<h3><br>\n");

#Conectamos con la BD, miramos si hay algun error(HACER)
my $dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";


#Insertamos los datos en la tabla, miramos si hay algun error(HACER)
$resultado = $dbh->do ("$SQL_inserta");

if ($resultado)
{

	print ("\n<h3>Cliente $user (Nombre=$nombre ) <b>Insertado</b><h3><br>\n");
}
else
{
	print "\n<h3>Cliente $user (Nombre=$nombre) <b>NO Insertado</b><h3><br>\n";
	print "<h2><b>ERROR</b> en inserci&oacute;n en $tabla_clientes: $DBI::errstr</h2>\n";
}

#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
escribe_final_html();

#Terminamos
exit;
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En ests funcion cogemos los parametros que son necesarios para el
#CGI.
sub coge_parametros()
{

        #Determinamos el tipo de metodo usado para pasar los argumentos
        $method=$ENV{"REQUEST_METHOD"};
        if ($method eq "GET")
        {
                #Si el metodo es GET los argumentos vienen en $ENV{"QUERY_STYRNG"}
                $argumento=$ENV{"QUERY_STRING"};
        }
        else
        {
                #Si el metodo es POST los argumentos vienen en la entrada estandar
                $argumento=<STDIN>;
        }

        # Obtengo las variables y las imprimo
        foreach (split(/&/,$argumento))
        {
                ($variable,$valor) = split(/=/, $_);
                $valor=~tr/+/ /;
                $valor=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
                $datos{$variable}=$valor;
        }

        $user=$datos{USER};
        $pass=$datos{PASS};
	$nombre=$datos{NOMBRE};
	$email=$datos{EMAIL};
	$direccion=$datos{DIRECCION};
	$telefono=$datos{TELEFONO};
}

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Inserción de cliente (CGI)</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#DDDDDD" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>INSERTAR CLIENTES (CGI)</h1>
                 </strong>

         </TD>
          <form action="cgi_lista_cliente.pl" method="post">
            <TABLE>
             <TR>
              <TD>
               <INPUT TYPE="submit" VALUE="Volver a lista clientes">
              </TD>
             </TR>
            </TABLE>
          </form>

        </TABLE>
inicio_HTML
}

#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------

