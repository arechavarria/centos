#!/usr/bin/perl
#!c:\perl\bin\perl 

#Usamos las librerias de acceso a BD
use DBI;

 $base_datos="CGI_Tarea_Seguridad";   #Nombre de las base de datos
 $tabla_clientes="clientes"; #Nombre de la tabla de clientes
 $usuario="root"; #Usuario de la BD
 $clave="adm123"; #Password de la BD
 $driver="mysql"; #Utilizamos el driver de mysql

#Creamos la sentencia SQL
 $SQL_consulta="select * from $tabla_clientes;";

#Escribimos la cabecera de la pagina HTML'#!/usr/bin/perl
#Escribimos la cabecera de la pagina HTML

print "Content-type: text/html\n\n";
escribe_inicio_html();

#Conectamos con la BD, miramos si hay algun error(HACER)
$dbh = DBI->connect("dbi:$driver:$base_datos",$usuario,$clave)
		|| die "\nError al abrir la base datos: $DBI::errstr\n";


#Hacemos una consulta en la tabla, y miramos si hay algun error(HACER)
#Mostramos los elementos de la tabla con la funcion muestra_cliente,
#cuyo formato es:
#                       muestra_cliente($id,$nombre,$precio);

 #Realizamos la etapa de preparaci�n de la sentencia
 $sth = $dbh->prepare("$SQL_consulta");

 #Realizamos la etapa de ejecuci�n de la sentencia
 $sth->execute();

 #Realizamos la etapa de extracción de datos. Imprimimos tupla a tupla.
 while ( @tupla=$sth->fetchrow_array() )
 {
        muestra_cliente($tupla[0],$tupla[1],$tupla[3],$tupla[4],$tupla[5]);
 }

 #Realizamos la etapa de liberaci�n de recursos ocupados por la sentencia
 $sth->finish();




#Nos desconectamos de la BD. (HACER)
$dbh->disconnect();

#Escribimos la cola de la pagina HTML
escribe_final_html();

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#----------------------------FUNCIONES-------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#En esta funcion muestra el cliente que se le pasa como parametro
#el formato es muestra_cliente($id,$nombre,$precio);
sub muestra_cliente()
{
        my ($id,$nombre,$email,$direccion,$telefono)=@_;

print <<cliente_HTML;

         <strong>
         <TD rowspan=2 valign=middle bgcolor="#008caa"><font color="#DDDDDD" face="sans-serif size=+2">$id</font></TD>
<TD rowspan=2 valign=middle bgcolor="#dddddd"><font color="#000000" face="sans-serif size=+2">$nombre</font></TD>
<TD rowspan=2 valign=middle bgcolor="#dddddd"><font color="#000000" face="sans-serif size=+2">$email</font></TD>
<TD rowspan=2 valign=middle bgcolor="#dddddd"><font color="#000000" face="sans-serif size=+2">$direccion</font></TD>
<TD rowspan=2 valign=middle bgcolor="#dddddd"><font color="#000000" face="sans-serif size=+2">$telefono</font></TD>
	 </strong>

	<!-- Boton de Borrar -->	
	 <TD valign=middle bgcolor="#ffffff">
		<FORM NAME="formBorrar$id" ACTION="cgi_borra_cliente.pl" METHOD=GET>
			<INPUT TYPE="hidden" NAME="ID" VALUE="$id">			
			<INPUT TYPE="button" VALUE="Borrar" onClick="document.formBorrar$id.submit()">
		</FORM>	
	</TD></TR>

	<!-- Linea en blanco-->
	<TR><TD colspan=3 valign=middle bgcolor="#FFFFFF"></TD></TR>
cliente_HTML
}

#Terminamos
exit;

#--------------------------------------------------------------------
#En esta funcion escribimos el principio de la pagina HTML
sub escribe_inicio_html()
{

print <<inicio_HTML;
<html>
        <HEAD>
                <TITLE>Listar clientes (CGI)</TITLE>
        </HEAD>
        <BODY bgcolor=#FFFFFF>
        <TABLE border=0  cellpadding=0 cellspacing=0 width=100%>
         <TD valign=middle width="100%" bgcolor="#000000">
                <font color="#ffffff" face="sans-serif">
                 <strong>
                        <h1 align=center style='text-align:center'>LISTAR CLIENTES (CGI)</h1>
                 </strong>
                </font>
         </TD>
        </TABLE>
	<hr>
	<br>
	<form action="ht_inserta_cliente.pl" method="post">
	<TABLE>
	<TR>
	<TD>

	<INPUT TYPE="submit" VALUE="volver a pantalla insertar">
	</TD>
	</TR>
	</TABLE>
	</form>

        <TABLE border=1  cellpadding=0 cellspacing=0 width=100%>
	<TR>
         <font color="#AAAAAA" face="sans-serif"><strong>
         <TH valign=middle bgcolor="#ff8000">Cliente</TH>
         <TH valign=middle bgcolor="#ff8000">User</TH>
         <TH valign=middle bgcolor="#ff8000">Name</TH>
         <TH valign=middle bgcolor="#ff8000">email</TH>
         <TH valign=middle bgcolor="#ff8000">Direccion</TH>
	 </strong></font>
	</TR>

inicio_HTML
}

#--------------------------------------------------------------------
#En esta funcion escribimos el fin de la pagina HTML
sub escribe_final_html()
{

print <<fin_HTML;
	</TABLE>
        </BODY>
        </HTML>
fin_HTML
}
#--------------------------------------------------------------------
